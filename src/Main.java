import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Storage management");
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        primaryStage.setFullScreen(false);
        primaryStage.setOnCloseRequest(e -> Platform.exit());

        primaryStage.setScene(createScene());
        primaryStage.show();
    }

    private Scene createScene() {
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(20));
        pane.setHgap(20);
        pane.setVgap(10);

        pane.add(new Text("Products"), 0, 0);

        ListView<String> prods_list = new ListView<>();
        prods_list.setOrientation(Orientation.VERTICAL);
        prods_list.setEditable(false);
        prods_list.setPrefWidth(150);
        ScrollPane usersPane = new ScrollPane(prods_list);
        usersPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        usersPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        usersPane.setFitToHeight(true);
        pane.add(usersPane, 0, 1);

        Button delete = new Button("Delete");
        delete.setAlignment(Pos.BOTTOM_LEFT);
        delete.setAlignment(Pos.CENTER);
        delete.setPrefWidth(75);
        pane.add(delete, 0, 2);

        Button edit = new Button("Edit");
        edit.setPrefWidth(75);
        edit.setAlignment(Pos.CENTER);
        pane.add(new HBox(0) {{
            setAlignment(Pos.BOTTOM_RIGHT);
            getChildren().add(edit);
        }}, 0, 2);

        pane.add(new Text("Modifications / Registers"), 1, 0);

        pane.add(inputs(), 1, 1, 1, 2);

        return new Scene(pane, 550, 400, Color.WHITE);
    }

    private GridPane inputs() {
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(10);

        Label code = new Label("Code:");
        code.setAlignment(Pos.CENTER_LEFT);
        pane.add(code, 0, 0);

        Label name = new Label("Name:");
        name.setAlignment(Pos.CENTER_LEFT);
        pane.add(name, 0, 1);

        Label cant = new Label("Quantity:");
        cant.setAlignment(Pos.CENTER_LEFT);
        pane.add(cant, 0, 2);

        Label type = new Label("Type:");
        type.setAlignment(Pos.CENTER_LEFT);
        pane.add(type, 0, 3);

        Label price = new Label("Price:");
        price.setAlignment(Pos.CENTER_LEFT);
        pane.add(price, 0, 4);

        Label manuf = new Label("Manufacturer:");
        manuf.setAlignment(Pos.CENTER_LEFT);
        pane.add(manuf, 0, 5);

        TextField code_inpt = new TextField();
        code_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(code_inpt, 1, 0);

        TextField name_inpt = new TextField();
        name_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(name_inpt, 1, 1);

        TextField cant_inpt = new TextField();
        cant_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(cant_inpt, 1, 2);

        TextField type_inpt = new TextField();
        type_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(type_inpt, 1, 3);

        TextField price_inpt = new TextField();
        price_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(price_inpt, 1, 4);

        TextField manuf_inpt = new TextField();
        manuf_inpt.setAlignment(Pos.CENTER_RIGHT);
        pane.add(manuf_inpt, 1, 5);

        GridPane grid = new GridPane();
        Button empty = new Button("Empty");
        empty.setPrefWidth(75);
        grid.add(new HBox(0) {{
            setAlignment(Pos.BOTTOM_RIGHT);
            getChildren().add(empty);
        }}, 0, 0);

        Button accept = new Button("Accept");
        accept.setPrefWidth(75);
        grid.add(new HBox(0) {{
            setAlignment(Pos.CENTER_RIGHT);
            setMargin(accept, new Insets(0, 100, 0, 0));
            getChildren().add(accept);
        }}, 0, 0);
        GridPane.setMargin(grid, new Insets(90, 0, 0, 0));
        pane.add(grid, 1, 6);

        return pane;
    }
}
